extends KinematicBody2D

var vel=Vector2()
export (int) var velocidad=100
var gravedad=600


func _ready():
	pass

func _physics_process(delta):
	vel.y+=gravedad*delta
	
	if is_on_wall():
		velocidad*=-1
	
	if vel.x>0:
		$spr.flip_h=false
	else:
		$spr.flip_h=true
	
	vel.x=velocidad
	vel=move_and_slide(vel,Vector2(0,-1))

func _on_punto_debil_body_entered(body):
	if body.is_in_group('bala_jugador'):
		queue_free()
