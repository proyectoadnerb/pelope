extends KinematicBody2D

var velocidad=Vector2()
var vel=100
var gravedad=600
var salto=200


var puede_saltar=false

var anim
var nueva_anim

func _ready():
	var exepciones=get_tree().get_nodes_in_group('enemigo')
	for i in exepciones:
		add_collision_exception_with(i)

func _physics_process(delta):
	velocidad.y+=gravedad*delta
	
	if global_position.y>300:
		global_position=Vector2(220,110)
	
	if Input.is_action_just_pressed("saltar") and puede_saltar:
		if velocidad.y>-250:
			velocidad.y=-salto
			puede_saltar=false
	
	
	
	if Input.is_action_pressed('derecha'):
		velocidad.x=vel
	elif Input.is_action_pressed('izquierda'):
		velocidad.x=-vel
	else:
		if velocidad.x!=0:
			if velocidad.x>0:
				velocidad.x-=5
			elif velocidad.x<0:
				velocidad.x+=5
		
	if is_on_floor():
		if velocidad.x!=0:
			nueva_anim='caminar'
			if velocidad.x>0:
				$spr.flip_h=false
			elif velocidad.x<0:
				$spr.flip_h=true
		else:
			nueva_anim='parado'

		if Input.is_action_just_pressed("saltar"):
			velocidad.y=-salto
			puede_saltar=true
	else:
		nueva_anim='saltar'
	
	if anim!=nueva_anim:
		anim=nueva_anim
		$anim.play(anim)
	
	velocidad=move_and_slide(velocidad,Vector2(0,-1))


func _on_detector_body_entered(body):
	if body.is_in_group('enemigo'):
		get_tree().get_nodes_in_group("sfx")[0].get_node('daño').play()
		if $spr.flip_h:
			velocidad=Vector2(200,-200)
		else:
			velocidad=Vector2(-200,-200)
		velocidad=move_and_slide(velocidad)